import pytest
from my_module import factorial


def test_factorial():
    assert factorial(0) == 1
    assert factorial(1) == 1
    assert factorial(5) == 120
    assert factorial(10) == 3628800


def test_factorial_negative_input():
    with pytest.raises(ValueError) as e:
        factorial(-1)
    assert str(e.value) == "Input must be a non-negative integer."
