import requests
from deploy import deploy


def test_deploy_success(monkeypatch, capsys):
    deploy()
    monkeypatch.setattr(requests, 'post', lambda url: MockResponse(200))
    captured = capsys.readouterr()
    assert captured.out == 'Deployment successful\n'


def test_deploy_failure(monkeypatch, capsys):
    deploy()
    monkeypatch.setattr(requests, 'post', lambda url: MockResponse(500))
    captured = capsys.readouterr()
    assert captured.out == 'Deployment failed\n'


class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code
